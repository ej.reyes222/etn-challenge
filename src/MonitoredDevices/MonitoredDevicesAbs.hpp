/*
 *  MonitoredDevicesAbs.hpp
 */

#ifndef MONDVCABS_H
#define MONDVCABS_H

#include <string>
#include "BusEntity/BusMock.hpp"
#include "MonitoredDevices/Device_Message.hpp"

using namespace std;

/* Device Status */
#define DEVICESTS_NORMAL    ((unsigned char) 0x00)
#define DEVICESTS_FAILURE   ((unsigned char) 0x01)

/* Device Message Flag */
#define DEVICEMSG_WITHOUT   ((unsigned char) 0)
#define DEVICEMSG_WITH      ((unsigned char) 1)

class MonitoredDevicesAbstraction {
    private:
    string deviceName;

    public:
    virtual void monitoredDevicesTask(BusMock*) = 0;
    const string getDeviceName(void);

};

#endif
