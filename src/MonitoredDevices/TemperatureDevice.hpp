/*
 *  TemperatureDevice.hpp
 */

#ifndef TEMPDVC_H
#define TEMPDVC_H

#include "MonitoredDevicesAbs.hpp"

class TemperatureDevice: public MonitoredDevicesAbstraction {
    
    private:
    const string deviceName = "Temperature Monitoring";

    public:
    void monitoredDevicesTask(BusMock* busInstance){

        DeviceMessageWrapped txmessage;
        
        txmessage.createTxMessage(MONITOREDDEVICESIDX_TEMPDEVICE, DEVICESTS_NORMAL, DEVICEMSG_WITH, (rand() % 65535));
        busInstance->sendBusData(txmessage);
    }

    const string getDeviceName(void) {
        return deviceName;
    }

};

#endif
