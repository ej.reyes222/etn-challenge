/*
 *  Device_Message.hpp
 */

#ifndef DVCMSG_H
#define DVCMSG_H

class DeviceMessageWrapped {

    private:
    unsigned char id;
    unsigned char msgSts;
    unsigned char msgRcvFlg;
    unsigned short msgData;

    public:
    void createTxMessage(unsigned char arg1, unsigned char arg2, unsigned char arg3, unsigned short arg4){
        id = arg1;
        msgSts = arg2;
        msgRcvFlg = arg3;
        msgData = arg4;

    }

    unsigned char getId(void){
        return id;
    }
    unsigned char getMsgRcvFlg(void){
        return msgRcvFlg;
    }
    unsigned char getMsgSts(void){
        return msgSts;
    }
    unsigned short getMsgData(void){
        return msgData;
    }
    
};

#endif
