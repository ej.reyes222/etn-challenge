/*
 *  VoltageDevice.hpp
 */

#ifndef VOLTDVC_H
#define VOLTDVC_H

#include "MonitoredDevicesAbs.hpp"

class VoltageDevice: public MonitoredDevicesAbstraction {

    private:
    const string deviceName = "Voltage Monitoring";

    public:
    void monitoredDevicesTask(BusMock* busInstance) {

        DeviceMessageWrapped txmessage;
        
        txmessage.createTxMessage(MONITOREDDEVICESIDX_VOLTDEVICE, DEVICESTS_NORMAL, DEVICEMSG_WITH, (rand() % 65535));
        busInstance->sendBusData(txmessage);
    }

    const string getDeviceName(void) {
        return deviceName;
    }

};

#endif
