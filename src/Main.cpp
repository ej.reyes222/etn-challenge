#include <iostream>
#include <stdio.h>
#include <random>
#include <cstdlib>
#include <ctime>

#include "MonitoredDevices/VoltageDevice.hpp"
#include "MonitoredDevices/TemperatureDevice.hpp"
#include "MonitoringDevice/MonitoringDevice.hpp"

using namespace std;

int main() {
    
    BusMock busMockInstance;
    VoltageDevice voltageDeviceInstance;
    TemperatureDevice temperatureDeviceInstance;
    MonitoringDevice monitoringDeviceInstance;

    /* SYSTEM INITIALIZATION */
    busMockInstance.initBus();
    monitoringDeviceInstance.initMonitorCnt();
    
    /* NORMAL SYSTEM PROCESSING */
    // while(1) {
    
    int setTimeFrame;
    cout << "Enter number of time frames: " << endl;
    cin >> setTimeFrame;
    for ( int timeFrames = 0 ; timeFrames < setTimeFrame; timeFrames++ ) {

        /* Cyclic Processes */

        srand(time(NULL));
        
        voltageDeviceInstance.monitoredDevicesTask(&busMockInstance);
        temperatureDeviceInstance.monitoredDevicesTask(&busMockInstance);
        monitoringDeviceInstance.monitoringTask(&busMockInstance);

        cout << "Bus Time Frame Count: " << (timeFrames + 1) << endl;
        cout << voltageDeviceInstance.getDeviceName() << " Message Count: " << monitoringDeviceInstance.getDeviceMsgRcvCnt(MONITOREDDEVICESIDX_VOLTDEVICE) << endl;
        cout << temperatureDeviceInstance.getDeviceName() << " Message Count: " <<  monitoringDeviceInstance.getDeviceMsgRcvCnt(MONITOREDDEVICESIDX_TEMPDEVICE) << endl;
        cout << endl;

        /* End of Cyclic Processes */

    }
    // } 
    
    return 0;

}
