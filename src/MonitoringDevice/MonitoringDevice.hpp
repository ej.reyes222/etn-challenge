/*
 *  MonitoringDevice.hpp
 */

#ifndef MNGDVCMOCK_H
#define MNGDVCMOCK_H

#include "BusEntity/BusMock.hpp"
#include "MonitoredDevices/MonitoredDevicesAbs.hpp"

#define INIT_MONITOR_CNT    ((unsigned long) 0)

class MonitoringDevice {
    
    private:
    unsigned long monitoringMsgRcvCnt[MONITOREDDEVICESCNT];

    public:
    void monitoringTask(BusMock*);
    void initMonitorCnt();
    unsigned long getDeviceMsgRcvCnt(unsigned char id);

};

#endif
