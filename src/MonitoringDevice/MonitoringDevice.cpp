/*
 *  MonitoringDevice.cpp
 */

#include <iostream>
#include <stdio.h>
#include <math.h>

#include "MonitoringDevice/MonitoringDevice.hpp"

using namespace std;

void MonitoringDevice::initMonitorCnt(void){

    for(unsigned char id = 0 ; id < MONITOREDDEVICESCNT ; id++){
        monitoringMsgRcvCnt[id] = INIT_MONITOR_CNT;
    }
}

void MonitoringDevice::monitoringTask(BusMock* busInstance){

    unsigned long busData;
    unsigned char deviceSts;
    unsigned long msgRcvFlag;

    busData = busInstance->getBusData();
    
    for(unsigned char id = 0 ; id < MONITOREDDEVICESCNT ; id++){

        deviceSts = (unsigned char)((busData >> DeviceBitInfoTbl[id].startBit) & BUS_DEVICESTS_SIZE);
        msgRcvFlag = (unsigned char)((busData >> (DeviceBitInfoTbl[id].startBit + BUS_MSGRCVFLG_OFFSET)) & BUS_MSGRCVFLG_SIZE);

        if (deviceSts != DEVICESTS_FAILURE) {
            if (msgRcvFlag == DEVICEMSG_WITH) {
                monitoringMsgRcvCnt[id]++;
            } else {
                ;
            }
        } else {
            ;
        }
    }
}

unsigned long MonitoringDevice::getDeviceMsgRcvCnt(unsigned char id){
    
    return monitoringMsgRcvCnt[id];
}
