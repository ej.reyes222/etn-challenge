/*
 *  BusMock.hpp
 */

#ifndef BUSMOCK_H
#define BUSMOCK_H

#include "BusEntity/BusMock_Configuration.hpp"
#include "MonitoredDevices/Device_Message.hpp"

class BusMock {
    
    private:
    unsigned long BUSDATA;
    
    public:
    void initBus(void);
    unsigned long getBusData(void);
    void sendBusData(DeviceMessageWrapped deviceMessage);

};

#endif
