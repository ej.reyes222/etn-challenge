/*
 *  BusMock_Configuration.hpp
 */

#ifndef BUSCONFIG_H
#define BUSCONFIG_H

/* Bus Initialization Value */
#define BUSDATA_INIT           ((unsigned long) 0x00000000)

/* Bus Reserved Bits Configuration */
#define BUS_RESERVEDBITS_MASK   (0xF000000F)

/* Device Message Auxiliary Settings */
#define BUS_DEVICESTS_SIZE      2
#define BUS_MSGRCVFLG_SIZE      1
#define BUS_MSGRCVFLG_OFFSET    (BUS_DEVICESTS_SIZE)
#define BUS_MSGDATA_OFFSET      (BUS_DEVICESTS_SIZE + BUS_MSGRCVFLG_SIZE)

/* Bit Assignments */
    /* Bit0 - Bit3 : Reserved */

    /* Bit4 - Bit14 : Device 1 */
#define BUS_DEVICE1_MSG_START      4

    /* Bit15 - Bit15 : Unused */

    /* Bit16 - Bit28 : Device 2 */
#define BUS_DEVICE2_MSG_START      16

    /* Bit29 - Bit32 : Reserved */


/* Device ID Registration */
enum {
    MONITOREDDEVICESIDX_VOLTDEVICE = 0,
    MONITOREDDEVICESIDX_TEMPDEVICE,

    MONITOREDDEVICESCNT

} MONITOREDDEVICESID;

/* Device Message Data Mask */
#define BUS_DEVICE1_MSG_MASK       (0x000000FF)
#define BUS_DEVICE2_MSG_MASK       (0x000003FF)

typedef struct {
    unsigned char startBit;
    unsigned short msgDataMask;
} DEVICEBITINF;

/* Device Info Table */
const DEVICEBITINF DeviceBitInfoTbl[MONITOREDDEVICESCNT] = {

        {BUS_DEVICE1_MSG_START, BUS_DEVICE1_MSG_MASK},     /* Device 1: Voltage Monitoring Device ; MONITOREDDEVICESIDX: MONITOREDDEVICESIDX_VOLTDEVICE */
        {BUS_DEVICE2_MSG_START, BUS_DEVICE2_MSG_MASK}      /* Device 2: Temperature Monitoring Device ; MONITOREDDEVICESIDX: MONITOREDDEVICESIDX_TEMPDEVICE */

};

#endif
