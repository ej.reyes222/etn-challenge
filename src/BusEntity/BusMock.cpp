/*
 *  BusMock.cpp
 */

#include "BusMock.hpp"

using namespace std;

void BusMock::initBus(void){
    BUSDATA = BUSDATA_INIT;
}

unsigned long BusMock::getBusData(void){
    return BUSDATA;
}

void BusMock::sendBusData(DeviceMessageWrapped deviceMessage) {

    unsigned long busData;
    
    busData = BUSDATA;

    busData &= ~((unsigned long)(0x03 << (DeviceBitInfoTbl[deviceMessage.getId()].startBit)));
    busData &= ~((unsigned long)(0x01 << (DeviceBitInfoTbl[deviceMessage.getId()].startBit + BUS_MSGRCVFLG_OFFSET)));
    busData &= ~((unsigned long)(DeviceBitInfoTbl[deviceMessage.getId()].msgDataMask << (DeviceBitInfoTbl[deviceMessage.getId()].startBit + BUS_MSGDATA_OFFSET)));

    busData |= (unsigned long)((deviceMessage.getMsgSts() & BUS_DEVICESTS_SIZE) << DeviceBitInfoTbl[deviceMessage.getId()].startBit);
    busData |= (unsigned long)((deviceMessage.getMsgRcvFlg() & BUS_MSGRCVFLG_SIZE) << (DeviceBitInfoTbl[deviceMessage.getId()].startBit + BUS_MSGRCVFLG_OFFSET));
    busData |= (unsigned long)((deviceMessage.getMsgData() & DeviceBitInfoTbl[deviceMessage.getId()].msgDataMask) << (DeviceBitInfoTbl[deviceMessage.getId()].startBit + BUS_MSGDATA_OFFSET));

    BUSDATA = busData;
}
